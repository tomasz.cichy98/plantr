import yaml
from gpiozero import MCP3008

from plantr.sensors.sensor import MoistureSensor, Sensor


class SensorFactory:
    def __init__(self):
        self.sensors_class_dict = {
            "MoistureSensor": MoistureSensor,
        }

    def create_sensor(self, config: dict) -> Sensor:
        if config["class"] != MoistureSensor:
            raise NotImplementedError("Only MoistureSensor implemented")

        sensor_cls = self.sensors_class_dict[config["class"]]
        kwargs = {
            "sensor_id": config["name"],
            "adc": MCP3008(channel=config["mcp_channel"], max_voltage=5.5),
            "scaling": tuple(config["scale"])
        }
        return sensor_cls(**kwargs)


if __name__ == "__main__":
    # Path to your YAML file
    file_path = './configs/sensors.yaml'

    factory = SensorFactory()
    with open(file_path, 'r') as file:
        # Load the YAML data
        data = yaml.safe_load(file)
    for sensor_config in data["sensors"]:
        sensor = factory.create_sensor(config=sensor_config)

    print(data)

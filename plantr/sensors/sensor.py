from abc import ABC, abstractmethod

from gpiozero import MCP3008


class Sensor(ABC):
    @abstractmethod
    def read(self) -> float:
        """Read the sensor value

        Returns:
            processed sensor value
        """
        pass


class MoistureSensor(Sensor):
    def __init__(self, sensor_id: str, adc: MCP3008, scaling: tuple = (0.3, 0.97)) -> None:
        self.sensor_id = sensor_id,
        self.adc = adc
        self.scaling = scaling
        self.target_scale = (0.0, 1.0)

    def read(self) -> float:
        """Read the sensor value

        Returns:
            processed sensor value
        """
        reading = self.adc.value
        reading = self._scale_reading(reading=reading)
        return reading

    def _scale_reading(self, reading: float) -> float:
        """Make sure the sensor reading is always scaled correctly

        Args:
            reading: raw reading from the sensor

        Returns:
            scaled reading
        """
        sensor_min, sensor_max = self.scaling
        target_min, target_max = self.target_scale

        scaled_value = ((reading - sensor_min) * (target_max - target_min)) / (sensor_max - sensor_min) + target_min
        scaled_value = max(min(scaled_value, target_max), target_min)
        return scaled_value

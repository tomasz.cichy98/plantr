from gpiozero import MCP3008
import time

# Create an MCP3008 object
sensors = {
    "moist_6": MCP3008(channel=6, max_voltage=5.5),
    "moist_7": MCP3008(channel=7, max_voltage=5.5)
}

try:
    while True:
        # Read the sensor value (range: 0.0 to 1.0)
        for name, sensor in sensors.items():
            sensor_value = sensor.value
            converted_value = int(sensor_value * 100)
            print(f"{name = }, {converted_value = }%")
        print()

        # Wait for a short duration before reading again
        time.sleep(1)

except KeyboardInterrupt:
    print("Exiting...")
